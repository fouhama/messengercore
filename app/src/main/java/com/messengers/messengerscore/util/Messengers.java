package com.messengers.messengerscore.util;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.apollographql.apollo.exception.ApolloException;
import com.messengers.messengerscore.util.cognito.LoginHelper;
import com.messengers.messengerscore.util.decryption.Decrypt;
import com.messengers.messengerscore.util.listener.OnCognitoistener;
import com.messengers.messengerscore.util.listener.OnConversationListListener;
import com.messengers.messengerscore.util.listener.OnLoginListener;
import com.messengers.messengerscore.util.listener.OnMsgListListener;
import com.messengers.messengerscore.util.listener.OnSubscriptionCompleteListener;
import com.messengers.messengerscore.util.listener.OnUnreadTotalCntListener;
import com.messengers.messengerscore.util.messenger.conversationlist.ConversationList;
import com.messengers.messengerscore.util.messenger.list.MessengerList;
import com.messengers.messengerscore.util.messenger.list.MessengerListUserVo;
import com.messengers.messengerscore.util.messenger.read.Reader;
import com.messengers.messengerscore.util.messenger.send.Sender;
import com.messengers.messengerscore.util.messenger.totalunreadcnt.ToatlUnreadCnt;
import com.messengers.messengerscore.util.query.UsersQuery;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class Messengers {
    static Messengers instance;
    private final LoginHelper loginHelper;

    private UsersQuery usersQuery;

    private Context mContext;
    private Context mCtx;

    private AWSAppSyncClient mAWSAppSyncClient;


    private  ConversationList conversation;
    private List<MessengerListUserVo> mUserList;


    public static Messengers getInstance(Context context) {
        if (instance == null) {
            instance = new Messengers(context);
        }
        return instance;
    }

    private Messengers(Context context) {
        mContext = context.getApplicationContext();
        mCtx = context;
        loginHelper = new LoginHelper(context);
        mAWSAppSyncClient = ClientFactory.getInstance(mAuthProvider , mContext);
        usersQuery = new UsersQuery(mCtx, mAuthProvider);
    }

    public boolean isCheck(){
        if (totalUnreadCnt != null && mMessengerList != null && conversation != null && sender != null && reader != null) {
            return true;
        }
        else {
            return false;
        }
    }

    public void executeQuery(final OnCognitoistener listsener){

        usersQuery.setListener(new OnCognitoistener() {
            @Override
            public void onSuccessCognito() {
                createObj();
                listsener.onSuccessCognito();
            }

            @Override
            public void onFailCognito(ApolloException e) {

            }
        });
    }

    public void login(String encrpt, OnLoginListener listener) {
        String id = "";
        String pw = "";
        try {
            JSONObject obj = new JSONObject( Decrypt.decrypt(encrpt, "jfouhamahaha") );
            id = obj.getString("username");
            pw = obj.getString("password");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        loginHelper.login(id, pw, listener);
    }


    ///////////////////////// Create Object Start////////////////////////////////
    private ToatlUnreadCnt totalUnreadCnt;
    private Sender sender;
    private Reader reader;

    private void createObj(){
        totalUnreadCnt = new ToatlUnreadCnt(mAWSAppSyncClient , usersQuery);
        sender = new Sender(mContext,mAuthProvider,usersQuery);
        reader = new Reader(mContext,mAuthProvider,usersQuery);
        mMessengerList = new MessengerList(mContext , mAWSAppSyncClient , mAuthProvider , usersQuery);
        conversation = new ConversationList( mContext , mAWSAppSyncClient , mAuthProvider , usersQuery );
    }
    ///////////////////////// Create Object End////////////////////////////////

    public void getTotalUnreadCnt(final OnUnreadTotalCntListener _listener){
//        if (usersQuery.getMyCognitoId() != null) {
//            totalUnreadCnt.setUnreadCntListListener(_listener);
//            totalUnreadCnt.getTotalCnt();
//        } else {
//            usersQuery.setListener(new OnCognitoistener() {
//                @Override
//                public void onSuccessCognito() {
//                    totalUnreadCnt.setUnreadCntListListener(_listener);
//                    totalUnreadCnt.getTotalCnt();
//                }
//
//                @Override
//                public void onFailCognito(ApolloException e) {
//
//                }
//            });
//        }

        if ( loginHelper.isValidToken() ) {
            totalUnreadCnt.setUnreadCntListListener(_listener);
            totalUnreadCnt.getTotalCnt();
        }

        else {

            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    totalUnreadCnt.setUnreadCntListListener(_listener);
                    totalUnreadCnt.getTotalCnt();
                }

                @Override
                public void onFailure(Exception e) {

                }
            });


        }


    }

    public void sendMessage(final String _mContent , final String _mConversationId){

        if(loginHelper.isValidToken()) {
            sender.send(_mContent , _mConversationId);
            conversation.sendMessage(_mContent);
        }

        else {

            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    sender.send(_mContent , _mConversationId);
                    conversation.sendMessage(_mContent);
                }

                @Override
                public void onFailure(Exception e) {

                }
            });

        }

    }

    public void readMessage(final String _mConversationId){
//        if (usersQuery.getMyCognitoId() != null) {
//            reader.read(_mConversationId);
//        } else {
//            usersQuery.setListener(new OnCognitoistener() {
//                @Override
//                public void onSuccessCognito() {
//                    reader.read(_mConversationId);
//                }
//
//                @Override
//                public void onFailCognito(ApolloException e) {
//
//                }
//            });
//        }

        if (loginHelper.isValidToken()) {
            reader.read(_mConversationId);
        }

        else {

            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    reader.read(_mConversationId);
                }

                @Override
                public void onFailure(Exception e) {

                }
            });

        }


    }

    public void setMessengerListUser(List<MessengerListUserVo> mList){
        this.mUserList = mList;
    }


    private MessengerList mMessengerList;
    private boolean messageCompleteFlag = true;
    public void messageList (final OnMsgListListener _mMsgListListener ){


        // 토큰이 살아 있을 경우
        if(loginHelper.isValidToken()) {
            mMessengerList.setMsgListListener( _mMsgListListener );
            mMessengerList.setCompleteFlag(messageCompleteFlag);
            mMessengerList.startList();

            mMessengerList.setSubScriptionListener(new OnSubscriptionCompleteListener() {
                @Override
                public void onComplete() {
                    messageCompleteFlag = false;
                    mMessengerList.destroy();
                    messageList(_mMsgListListener);
                }
            });
        }

        // 토큰 만료 재 로그인
        else {

            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    mMessengerList.setMsgListListener( _mMsgListListener );
                    mMessengerList.setCompleteFlag(messageCompleteFlag);
                    mMessengerList.startList();

                    mMessengerList.setSubScriptionListener(new OnSubscriptionCompleteListener() {
                        @Override
                        public void onComplete() {
                            messageCompleteFlag = false;
                            mMessengerList.destroy();
                            messageList(_mMsgListListener);
                        }
                    });
                }
                @Override
                public void onFailure(Exception e) {
                }
            });
        }
    }

    public void messageNextList(){

        // 토큰이 살아 있을 경우
        if(loginHelper.isValidToken()){
            mMessengerList.getNextMsgList();
        }

        // 토큰 만료 재로그인
        else {
            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    mMessengerList.getNextMsgList();
                }

                @Override
                public void onFailure(Exception e) {

                }
            });
        }

    }


    public void conversationList ( final String _mConversationId , final OnConversationListListener _mConversationListListener  ) {


        // 토큰이 살아 있을 경우
        if(loginHelper.isValidToken()){
            conversation.setConversationUserInfo(mUserList);
            conversation.setConversationId(_mConversationId);
            conversation.setConversationListListener( _mConversationListListener );
            conversation.setStartList();
        }

        // 토큰 만료 재 로그인
        else {

            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    conversation.setConversationUserInfo(mUserList);
                    conversation.setConversationId(_mConversationId);
                    conversation.setConversationListListener( _mConversationListListener );
                    conversation.setStartList();
                }
                @Override
                public void onFailure(Exception e) {

                }
            });
        }
    }



    public void unbindConversation(){

        // 토큰이 살아 있을 경우
        if( loginHelper.isValidToken() ) {
            conversation.stopSubScription();
        }

        // 토큰 만료 재 로그인
        else {
            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    conversation.stopSubScription();
                }

                @Override
                public void onFailure(Exception e) {

                }
            });
        }
    }

    public void conversationNextList(){
        // 토큰이 살아 있을 경우
        if (loginHelper.isValidToken()) {
            conversation.getNextConversationList();
        }

        // 토큰 만료 재 로그인
        else {
            loginHelper.relogin(new OnLoginListener() {
                @Override
                public void onLogin() {
                    conversation.getNextConversationList();
                }

                @Override
                public void onFailure(Exception e) {

                }
            });

        }
    }
    public CognitoUserPoolsAuthProvider getMauthProvider(){
        return mAuthProvider;
    }

    private CognitoUserPoolsAuthProvider mAuthProvider = new CognitoUserPoolsAuthProvider() {
        @Override
        public String getLatestAuthToken() {
            return loginHelper.getToken();
        }
    };
}