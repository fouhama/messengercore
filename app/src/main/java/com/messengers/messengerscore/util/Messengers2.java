//package com.messengers.messengerscore.util;
//
//import android.content.Context;
//
//import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
//import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
//import com.apollographql.apollo.exception.ApolloException;
//import com.messengers.messengerscore.util.cognito.LoginHelper;
//import com.messengers.messengerscore.util.decryption.Decrypt;
//import com.messengers.messengerscore.util.listener.OnCognitoistener;
//import com.messengers.messengerscore.util.listener.OnConversationListListener;
//import com.messengers.messengerscore.util.listener.OnLoginListener;
//import com.messengers.messengerscore.util.listener.OnMsgListListener;
//import com.messengers.messengerscore.util.listener.OnSubscriptionCompleteListener;
//import com.messengers.messengerscore.util.listener.OnUnreadTotalCntListener;
//import com.messengers.messengerscore.util.messenger.conversationlist.ConversationList;
//import com.messengers.messengerscore.util.messenger.list.MessengerList;
//import com.messengers.messengerscore.util.messenger.list.MessengerListUserVo;
//import com.messengers.messengerscore.util.messenger.read.Reader;
//import com.messengers.messengerscore.util.messenger.send.Sender;
//import com.messengers.messengerscore.util.messenger.totalunreadcnt.ToatlUnreadCnt;
//import com.messengers.messengerscore.util.query.UsersQuery;
//import com.messengers.messengerscore.util.query.UsersQuery2;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.util.List;
//
//interface Test {
//    void func();
//}
//
//public class Messengers2  {
//    private Test t;
//    public void setT(Test t) {
//        this.t = t;
//    }
//
//    static Messengers2 instance;
//    private final LoginHelper loginHelper;
//
//    private UsersQuery2 usersQuery2;
//
//    private Context mContext;
//    private Context mCtx;
//
//    private AWSAppSyncClient mAWSAppSyncClient;
//
//
//    private  ConversationList conversation;
//    private List<MessengerListUserVo> mUserList;
//
//
//    public static Messengers2 getInstance(Context context) {
//        if (instance == null) {
//            instance = new Messengers2(context);
//        }
//        return instance;
//    }
//
//    private Messengers2(Context context) {
//        mContext = context.getApplicationContext();
//        mCtx = context;
//        loginHelper = new LoginHelper(context);
//        mAWSAppSyncClient = ClientFactory.getInstance( mAuthProvider , mContext);
//        usersQuery2 = new UsersQuery2(mCtx, mAuthProvider);
//    }
//
//    public void login(String encrpt, OnLoginListener listener) {
//        String id = "";
//        String pw = "";
//        try {
//            JSONObject obj = new JSONObject( Decrypt.decrypt(encrpt, "jfouhamahaha") );
//            id = obj.getString("username");
//            pw = obj.getString("password");
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        loginHelper.login(id, pw, listener);
//    }
//
//    ///////////////////////// Total Unread Cnt Start ////////////////////////////////
//    private ToatlUnreadCnt totalUnreadCnt;
//    public void getUnreadTotalCnt( final OnUnreadTotalCntListener _listener ){
////        usersQuery2.query(new OnCognitoistener() {
////            @Override
////            public void onSuccessCognito() {
////                if (t != null) {
////                    t.func();
////                }
////            }
////
////            @Override
////            public void onFailCognito(ApolloException e) {
////
////            }
////        });
////        if ( usersQuery == null ) {
////            usersQuery = new UsersQuery(mCtx, mAuthProvider, new OnCognitoistener() {
////                @Override
////                public void onSuccessCognito() {
////                    createTotalUnreadCntObj(_listener);
////                }
////                @Override
////                public void onFailCognito(ApolloException e) { }
////            });
////        }
//
////        else {
////            createTotalUnreadCntObj(_listener);
////        }
//    }
//
//    private void createTotalUnreadCntObj(OnUnreadTotalCntListener _listener){
//        totalUnreadCnt = new ToatlUnreadCnt(mAWSAppSyncClient , usersQuery2);
//        totalUnreadCnt.setUnreadCntListListener(_listener);
//        totalUnreadCnt.getTotalCnt();
//    }
//    ///////////////////////// Total Unread Cnt END ///////////////////////////////////
//
//
//    public void sendMessage(String _mContent , String _mConversationId){
//        Sender sender = new Sender(mContext , mAuthProvider , _mContent , _mConversationId);
//        sender.send();
//    }
//
//    public void readMessage(final String _mConversationId){
//
//        if ( usersQuery2 == null ) {
//            usersQuery2 = new UsersQuery2(mCtx, mAuthProvider, new OnCognitoistener() {
//                @Override
//                public void onSuccessCognito() {
//                    Reader reader = new Reader(mContext , mAuthProvider , _mConversationId , usersQuery2);
//                    reader.read();
//                }
//
//                @Override
//                public void onFailCognito(ApolloException e) {
//
//                }
//            });
//        }
//
//        else {
//            Reader reader = new Reader(mContext , mAuthProvider , _mConversationId , usersQuery2);
//            reader.read();
//        }
//    }
//
//
//
//
//
//
//    public void setMessengerListUser(List<MessengerListUserVo> mList){
//        this.mUserList = mList;
//    }
//
//
//    private MessengerList mMessengerList;
//    private boolean messageCompleteFlag = true;
//    public void messageList ( final OnMsgListListener _mMsgListListener ){
//        if ( usersQuery2== null ) {
//            usersQuery2 = new UsersQuery2(mCtx, mAuthProvider, new OnCognitoistener() {
//                @Override
//                public void onSuccessCognito() {
//                    mMessengerList = new MessengerList(mContext , mAWSAppSyncClient , mAuthProvider , usersQuery2 , messageCompleteFlag);
//                    mMessengerList.setMsgListListener( _mMsgListListener );
//                    mMessengerList.setSubScriptionListener(new OnSubscriptionCompleteListener() {
//                        @Override
//                        public void onComplete() {
//                            messageCompleteFlag = false;
//                            messageList(_mMsgListListener);
//                        }
//                    });
//                }
//
//                @Override
//                public void onFailCognito(ApolloException e) {
//
//                }
//            });
//        }
//
//        else {
//            mMessengerList = new MessengerList(mContext , mAWSAppSyncClient , mAuthProvider , usersQuery2 , messageCompleteFlag);
//            mMessengerList.setMsgListListener( _mMsgListListener );
//            mMessengerList.setSubScriptionListener(new OnSubscriptionCompleteListener() {
//                @Override
//                public void onComplete() {
//                    messageCompleteFlag = false;
//                    mMessengerList.destroy();
//                    mMessengerList = null;
//                    messageList(_mMsgListListener);
//                }
//            });
//        }
//    }
//
//    public void messageNextList(){
//        mMessengerList.getNextMsgList();
//    }
//
//
//    public void conversationList ( final String _mConversationId , final OnConversationListListener _mConversationListListener  ) {
//
//        if ( usersQuery2 == null ) {
////            usersQuery2.query(new OnCognitoistener() {
////                @Override
////                public void onSuccessCognito() {
////                    if (listener != null) {
////                        lissner/opn
////                    }
////                }
////
////                @Override
////                public void onFailCognito(ApolloException e) {
////
////                }
////            });
//            usersQuery2 = new UsersQuery2(mCtx, mAuthProvider, new OnCognitoistener() {
//                @Override
//                public void onSuccessCognito() {
//                    conversation = new ConversationList( mContext , mAWSAppSyncClient , mAuthProvider , usersQuery2 , _mConversationId );
//                    conversation.setConversationUserInfo(mUserList);
//                    conversation.setConversationListListener( _mConversationListListener );
//                }
//
//                @Override
//                public void onFailCognito(ApolloException e) {
//
//                }
//            });
//        }
//
//        else {
//            conversation = new ConversationList( mContext , mAWSAppSyncClient , mAuthProvider , usersQuery2 , _mConversationId );
//            conversation.setConversationUserInfo(mUserList);
//            conversation.setConversationListListener( _mConversationListListener );
//        }
//    }
//
//    public void unbindConversation(){
//        conversation.stopSubScription();
//    }
//
//    public void conversationNextList(){
//        conversation.getNextConversationList();
//    }
//
//
//    public CognitoUserPoolsAuthProvider getMauthProvider(){
//        return mAuthProvider;
//    }
//
//    private CognitoUserPoolsAuthProvider mAuthProvider = new CognitoUserPoolsAuthProvider() {
//        @Override
//        public String getLatestAuthToken() {
//            return loginHelper.getToken();
//        }
//    };
//
//}
