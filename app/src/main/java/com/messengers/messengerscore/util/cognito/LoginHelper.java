package com.messengers.messengerscore.util.cognito;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.AuthenticationDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.ChallengeContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.continuations.MultiFactorAuthenticationContinuation;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.AuthenticationHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.messengers.messengerscore.util.listener.OnLoginListener;

import java.util.Locale;


public class LoginHelper {
    Context mContext;
    OnLoginListener listener;


    public LoginHelper(Context context) {
        mContext = context.getApplicationContext();
        AppHelper.init(mContext);
    }

    private String username , userpw;
    private CognitoUser user;

    public void login(String id, String pw , OnLoginListener listener) {
        user = AppHelper.getPool().getCurrentUser();
        username = id;
        userpw = pw;
        AppHelper.setUser(username);

        this.listener = listener;
        user.getSessionInBackground(authenticationHandler);
    }

    public void relogin(OnLoginListener _listener){
        user.getSession(authenticationHandler);
        this.listener = _listener;
    }


    AuthenticationHandler authenticationHandler = new AuthenticationHandler() {
        @Override
        public void onSuccess(CognitoUserSession cognitoUserSession, CognitoDevice device) {
            AppHelper.setCurrSession(cognitoUserSession);
            AppHelper.newDevice(device);

            if (device != null) {
                device.rememberThisDeviceInBackground(trustedDeviceHandler);
            }

            if (listener != null) {
                listener.onLogin();
                listener = null;
            }
        }

        @Override
        public void getAuthenticationDetails(AuthenticationContinuation authenticationContinuation, String username) {
            Locale.setDefault(Locale.US);
            getUserAuthentication(authenticationContinuation, username);
        }

        @Override
        public void onFailure(Exception e) {
            e.printStackTrace();
            if (listener != null) {
                listener.onFailure(e);
                listener = null;
            }
        }

        @Override
        public void getMFACode(MultiFactorAuthenticationContinuation multiFactorAuthenticationContinuation) {}
        @Override
        public void authenticationChallenge(ChallengeContinuation continuation) {}
    };

    private void getUserAuthentication(AuthenticationContinuation continuation, String username) {
        AuthenticationDetails authenticationDetails = new AuthenticationDetails(this.username, this.userpw, null);
        continuation.setAuthenticationDetails(authenticationDetails);
        continuation.continueTask();
    }

    public String getToken() {
        return AppHelper.getCurrSession().getAccessToken().getJWTToken();
    }

    public boolean isValidToken(){
        return AppHelper.getCurrSession().isValid();
    }

    GenericHandler trustedDeviceHandler = new GenericHandler() {
        @Override
        public void onSuccess() {
        }

        @Override
        public void onFailure(Exception exception) {
        }
    };
}
