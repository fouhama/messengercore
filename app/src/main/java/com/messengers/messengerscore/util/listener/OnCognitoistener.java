package com.messengers.messengerscore.util.listener;

import com.apollographql.apollo.exception.ApolloException;

public interface OnCognitoistener {
    void onSuccessCognito();
    void onFailCognito(ApolloException e);

}
