package com.messengers.messengerscore.util.listener;

import com.apollographql.apollo.exception.ApolloException;
import com.messengers.messengerscore.util.messenger.conversationlist.ConversationList;
import com.messengers.messengerscore.util.messenger.conversationlist.ConversationListVo;
import com.messengers.messengerscore.util.messenger.list.MessengerListVo;

import java.util.List;

import javax.annotation.Nullable;

public interface OnConversationListListener {
    void onResponse(List<ConversationListVo> list );
    void onFailure(@Nullable ApolloException e);
}
