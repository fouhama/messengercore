package com.messengers.messengerscore.util.listener;

public interface OnLoginListener {
    void onLogin();
    void onFailure(Exception e);
}
