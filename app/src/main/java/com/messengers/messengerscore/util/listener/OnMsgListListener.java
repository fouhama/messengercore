package com.messengers.messengerscore.util.listener;

import com.apollographql.apollo.exception.ApolloException;
import com.messengers.messengerscore.util.messenger.list.MessengerListVo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;

public interface OnMsgListListener {
    void onResponse(List<MessengerListVo> list);
    void onFailure( @Nullable  ApolloException e);
}
