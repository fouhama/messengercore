package com.messengers.messengerscore.util.listener;

public interface OnSubscriptionCompleteListener {
    void onComplete();
}
