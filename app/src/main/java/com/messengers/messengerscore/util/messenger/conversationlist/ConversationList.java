package com.messengers.messengerscore.util.messenger.conversationlist;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.messengers.graphql.CreateMessageMutation;
import com.messengers.graphql.GetMessageQuery;
import com.messengers.graphql.MessageSubscription;
import com.messengers.messengerscore.util.ClientFactory;
import com.messengers.messengerscore.util.listener.OnConversationListListener;
import com.messengers.messengerscore.util.messenger.list.MessengerListUserVo;
import com.messengers.messengerscore.util.messenger.list.MessengerListVo;
import com.messengers.messengerscore.util.query.UsersQuery;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;

public class ConversationList {

    private Context mCtx;
    private AWSAppSyncClient mAWSAppSyncClient;
    private OnConversationListListener mConversationListListener;
    private CognitoUserPoolsAuthProvider mAuthProvider;
    private UsersQuery mUsersQuery;

    private List<ConversationListVo> result = new ArrayList<ConversationListVo>();

    private String myCognitoId;
    private List<MessengerListUserVo> userList;
    private String conversationId;
    private AppSyncSubscriptionCall<MessageSubscription.Data> subscriptionWatcher;

    public ConversationList(Context _mCtx , AWSAppSyncClient _mAWSAppSyncClient , CognitoUserPoolsAuthProvider _mAuthProvider , UsersQuery _mUsersQuery ){
        this.mCtx = _mCtx;
        this.mAWSAppSyncClient = _mAWSAppSyncClient;
        this.mAuthProvider = _mAuthProvider;
        this.mUsersQuery = _mUsersQuery;
        myCognitoId = mUsersQuery.getMyCognitoId();

    }

    public void setStartList(){
        startSubscription();
        getConversationList(conversationId);
    }

    public void setConversationId( String _mConversationId){
        this.conversationId = _mConversationId;
    }


    public void setConversationListListener(OnConversationListListener _mMconversationListListener){
        this.mConversationListListener = _mMconversationListListener;
    }

    public void setConversationUserInfo( List<MessengerListUserVo> _mUserList ){
        this.userList = _mUserList;
    }


    private void startSubscription() {
        MessageSubscription subscription = MessageSubscription.builder()
                .conversationId(conversationId)
                .build();

        subscriptionWatcher = ClientFactory.getInstance(mAuthProvider, mCtx.getApplicationContext()).subscribe(subscription);
        subscriptionWatcher.execute(subscriptionCallback);
    }

    public void stopSubScription(){
        result = new ArrayList<ConversationListVo>();
        subscriptionWatcher.cancel();
    }

    private AppSyncSubscriptionCall.Callback<MessageSubscription.Data> subscriptionCallback = new AppSyncSubscriptionCall.Callback<MessageSubscription.Data>() {
        @Override
        public void onResponse(final @Nonnull Response<MessageSubscription.Data> response) {

            String content = response.data().subscribeToMessage().content();
            String sender  = response.data().subscribeToMessage().sender();
            String createAt = response.data().subscribeToMessage().createdAt();
            String id = response.data().subscribeToMessage().id();
            Boolean isSend = response.data().subscribeToMessage().isSent();

            ConversationListVo listVo = new ConversationListVo();
            listVo.setContent(content);
            listVo.setCreateAt(createAt);
            listVo.setSent(isSend);
            listVo.setSenderId(sender);

            //addPostOffline(response.data());

            if ( sender.equals( myCognitoId )) {
                listVo.setMeChk(true);
            }
            else{
                listVo.setMeChk(false);
            }
            for ( MessengerListUserVo userVo : userList ) {
                if( sender.equals( userVo.getId() ) ){
                    listVo.setSenderNm( userVo.getUsername() );
                    listVo.setThumbImgUrl( userVo.getProfileImage() );
                    break;
                }
            }

            int i = 0;
            boolean addFlag = true;
            for ( ConversationListVo vo : result) {
                if ( !vo.isSent() && vo.getContent().equals(content) ) {
                    addFlag = false;
                    result.set(i , listVo);
                }
                i ++;
            }

            if (addFlag) {
                result.add( 0, listVo );
            }

            addPostOffline(response.data());
            mConversationListListener.onResponse(result);
        }

        @Override
        public void onFailure(final @Nonnull ApolloException e) {
            Log.e("SUBSCRIPTION_RESPONSE", "Conversation Subscription failure" + e.getStackTrace());
            e.getStackTrace();
        }

        @Override
        public void onCompleted() {
            Log.d("SUBSCRIPTION_RESPONSE", "Converation Subscription completed");
        }
    };

    private void addPostOffline(final MessageSubscription.Data addPostData) {
        final AWSAppSyncClient appSyncClient = ClientFactory.getInstance(mAuthProvider , mCtx);
        final GetMessageQuery allPostsQuery = GetMessageQuery.builder().conversationId(conversationId).build();
        appSyncClient.query(allPostsQuery)
                .responseFetcher(AppSyncResponseFetchers.CACHE_ONLY)
                .enqueue(new GraphQLCall.Callback<GetMessageQuery.Data>() {
                    @Override
                    public void onResponse(@Nonnull Response<GetMessageQuery.Data> response) {

                        List<GetMessageQuery.Message> oldPostsList = response.data().allMessageConnection().messages();
                        List<GetMessageQuery.Message> newPostsList = new ArrayList<>(oldPostsList);


                        String senderId = addPostData.subscribeToMessage().id();

                        boolean compareIdFlag = false;
                        int id = 0;

                        for (GetMessageQuery.Message message : newPostsList){
                            if (message.id().equals(senderId)){
                                compareIdFlag = true;
                                id += 1;
                                break;
                            }
                        }

                        // 새로 들어 왔을 경우 추가 ( 상대방이 보냈을 경우 )
                        if (!compareIdFlag) {
                            newPostsList.add(0, new GetMessageQuery.Message(
                                    addPostData.subscribeToMessage().__typename(),
                                    addPostData.subscribeToMessage().id(),
                                    addPostData.subscribeToMessage().content(),
                                    addPostData.subscribeToMessage().isSent(),
                                    addPostData.subscribeToMessage().createdAt(),
                                    addPostData.subscribeToMessage().sender()
                            ));
                        }

                        // 본인이 이미 보냈을 경우 캐쉬에 담겨 있기 때문에
                        else {
                            newPostsList.set(id, new GetMessageQuery.Message(
                                    addPostData.subscribeToMessage().__typename(),
                                    addPostData.subscribeToMessage().id(),
                                    addPostData.subscribeToMessage().content(),
                                    addPostData.subscribeToMessage().isSent(),
                                    addPostData.subscribeToMessage().createdAt(),
                                    addPostData.subscribeToMessage().sender()
                            ));
                        }
                        GetMessageQuery.Data data =
                                new GetMessageQuery.Data(new GetMessageQuery.AllMessageConnection(
                                        response.data().allMessageConnection().__typename(), newPostsList, null));

                        try {
                            appSyncClient.getStore().write(allPostsQuery, data).execute();
                        } catch (ApolloException e) {
                            Log.e("As", "Failed to update ListPosts query optimistically", e);
                        }
                    }
                    @Override
                    public void onFailure(@Nonnull ApolloException e) {
                        Log.e("as", "Failed to update ListPosts query optimistically", e);
                    }
                });
    }




    // Conversation first List Query
    private void getConversationList(String conversationId){
        mAWSAppSyncClient.query(
                GetMessageQuery
                        .builder()
                        .conversationId(conversationId)
                        .build()
        )
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(eventsCallback);
    }


    // Conversation next List Query
    public void getNextConversationList() {
        if ( nextToken != null ){
            mAWSAppSyncClient.query(
                    GetMessageQuery
                            .builder()
                            .after(nextToken)
                            .conversationId(conversationId)
                            .build()
            )
                    .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                    .enqueue(eventsCallback);
        }
    }

    public void sendMessage(String content){
        ConversationListVo vo = new ConversationListVo();
        vo.setMeChk(true);
        vo.setSent(false);
        vo.setCache(false);
        vo.setContent(content);
        result.add(0 , vo);
        mConversationListListener.onResponse(result);
    }



    private String nextToken = null;
    private GraphQLCall.Callback<GetMessageQuery.Data> eventsCallback = new GraphQLCall.Callback<GetMessageQuery.Data>() {
        @Override
        public void onResponse(@Nonnull Response<GetMessageQuery.Data> response) {
            if (response.data() != null) {

                //Network 일 경우 cache 리스트 제거
                if ( !response.fromCache() ) {
                    Iterator<ConversationListVo> i = result.iterator();
                    while (i.hasNext()) {
                        boolean cache = i.next().isCache();
                        if (cache) {
                            i.remove();
                        }
                    }
                }


                List<GetMessageQuery.Message> mConversationList;
                mConversationList = response.data().allMessageConnection().messages();
                for ( GetMessageQuery.Message obj : mConversationList ) {
                    ConversationListVo listVo = new ConversationListVo();
                    listVo.setContent( obj.content());
                    listVo.setCreateAt( obj.createdAt());
                    listVo.setSent(obj.isSent());
                    listVo.setSenderId( obj.sender());
                    listVo.setCache(response.fromCache());

                    if ( obj.sender().equals( myCognitoId )) {
                        listVo.setMeChk(true);
                    }
                    else{
                        listVo.setMeChk(false);
                    }

                    for ( MessengerListUserVo userVo : userList ) {
                        if( obj.sender().equals( userVo.getId() ) ){
                            listVo.setSenderNm(userVo.getUsername());
                            listVo.setThumbImgUrl(userVo.getProfileImage());
                            break;
                        }
                    }

                    result.add( listVo );
                }

                nextToken = response.data().allMessageConnection().nextToken();
                mConversationListListener.onResponse(result);

            } else {
                //events = new ArrayList<>();
            }
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            mConversationListListener.onFailure(e);
        }
    };
}
