package com.messengers.messengerscore.util.messenger.list;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.AppSyncSubscriptionCall;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.fetcher.ResponseFetcher;
import com.messengers.graphql.GetMyConversationQuery;
import com.messengers.graphql.SubscribeToUCsSubscription;
import com.messengers.messengerscore.util.ClientFactory;
import com.messengers.messengerscore.util.listener.OnCognitoistener;
import com.messengers.messengerscore.util.listener.OnMsgListListener;
import com.messengers.messengerscore.util.listener.OnSubscriptionCompleteListener;
import com.messengers.messengerscore.util.query.UsersQuery;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Nonnull;

public class MessengerList {

    private Context mCtx;
    private AWSAppSyncClient mAWSAppSyncClient;
    private OnMsgListListener mMsgListListener;
    private OnSubscriptionCompleteListener mSubScriptionListener;
    private CognitoUserPoolsAuthProvider mAuthProvider;
    private AppSyncSubscriptionCall<SubscribeToUCsSubscription.Data> subscriptionWatcher;
    private UsersQuery mUsersQuery;
    private List<MessengerListVo> result = new ArrayList<MessengerListVo>();
    private String nextToken = null;
    private boolean mOnCompleteFlag;


    public static int messengerListCnt = 0;


    public MessengerList(Context _mCtx , AWSAppSyncClient _mAWSAppSyncClient , CognitoUserPoolsAuthProvider _mAuthProvider , UsersQuery _mUsersQuery){
        this.mCtx = _mCtx;
        this.mAWSAppSyncClient = _mAWSAppSyncClient;
        this.mAuthProvider = _mAuthProvider;
        this.mUsersQuery = _mUsersQuery;
        //this.mOnCompleteFlag = _completeFlag;
        //startSubscription();
        //getMsgList();
    }

    public void setCompleteFlag(boolean _completeFlag){
        this.mOnCompleteFlag = _completeFlag;
    }

    public void startList(){
        startSubscription();
        getMsgList();
    }



    public void destroy(){
        result = new ArrayList<MessengerListVo>();
        if (subscriptionWatcher != null) {
            subscriptionWatcher.cancel();
        }
    }

    public void setMsgListListener (OnMsgListListener _mMsgListListener){
        this.mMsgListListener = _mMsgListListener;
    }

    public void setSubScriptionListener( OnSubscriptionCompleteListener _listenr){
        this.mSubScriptionListener = _listenr;
    }


    private void startSubscription() {


        Log.e("SUBSCRIPTION_RESPONSE" , "Create SubScription Cnt :: " + messengerListCnt);

        messengerListCnt ++;

        SubscribeToUCsSubscription subscription = SubscribeToUCsSubscription.builder()
                .userId(mUsersQuery.getMyCognitoId())
                .build();
        subscriptionWatcher = ClientFactory.getInstance(mAuthProvider, mCtx.getApplicationContext()).subscribe(subscription);
        subscriptionWatcher.execute(subscriptionCallback);

    }

    private AppSyncSubscriptionCall.Callback<SubscribeToUCsSubscription.Data> subscriptionCallback = new AppSyncSubscriptionCall.Callback<SubscribeToUCsSubscription.Data>() {
        @Override
        public void onResponse(final @Nonnull Response<SubscribeToUCsSubscription.Data> response) {

            String conversationId = response.data().subscribeToUCs().conversationId();
            String lastContent = response.data().subscribeToUCs().lastMessage();
            String liastMsgTime = response.data().subscribeToUCs().lastMessageTime();
            int unreadCnt = response.data().subscribeToUCs().unreadcount();

            List<SubscribeToUCsSubscription.Associated> associatedList = response.data().subscribeToUCs().associated();

            int i = 0;
            for (MessengerListVo listVo : result ) {
                // 같을 경우 맨 위로
                if ( listVo.getConversationId().equals( conversationId ) ) {
                    break;
                }
                i++;
            }

            // 새로 추가 된 메시지 일 경우
            if ( result.size() <= i ) {
                MessengerListVo listVo = new MessengerListVo();
                listVo.setConversationId(conversationId);
                listVo.setUnreadcount(unreadCnt );
                listVo.setLastMessageTime(liastMsgTime);
                listVo.setLastMessage(lastContent);

                ArrayList<MessengerListUserVo> userList = new ArrayList<MessengerListUserVo>();
                for ( SubscribeToUCsSubscription.Associated userObj : associatedList ){

                    MessengerListUserVo listUserVo = new MessengerListUserVo();
                    listUserVo.setId(userObj.user().cognitoId());
                    listUserVo.setProfileImage(userObj.user().profileImage());
                    listUserVo.setUsername(userObj.user().username());
                    listUserVo.setCustomData(userObj.user().customData());

                    if ( userObj.user().cognitoId().equals(mUsersQuery.getMyCognitoId())) {
                        listUserVo.setMe(true);
                    }
                    else {
                        listUserVo.setMe(false);
                    }

                    userList.add(listUserVo);

                }
                listVo.setUserVo( userList );

                result.add(0, listVo);
            }

            // 기존에 있는 경우 맨 위로
            else {
                MessengerListVo listVo = result.get(i);
                listVo.setLastMessage(lastContent);
                listVo.setLastMessageTime(liastMsgTime);
                listVo.setConversationId(conversationId);
                listVo.setUnreadcount(unreadCnt);
                if ( unreadCnt != 0 ) {
                    result.remove(i);
                    result.add(0, listVo);
                }
            }
            mMsgListListener.onResponse(result);
        }

        @Override
        public void onFailure(final @Nonnull ApolloException e) {
            Log.e("SUBSCRIPTION_RESPONSE", "mOnCompleteFlag ::  " + mOnCompleteFlag + "MessengerList Subscription failure " + e.getMessage());

            if ( mSubScriptionListener != null && e.getMessage().equals("Failed to execute http call")) {

                if ( mSubScriptionListener != null && test ) {
                    test = false;
                    mSubScriptionListener.onComplete();
                }
            }
        }

        @Override
        public void onCompleted() {
            if ( mSubScriptionListener == null )
                Log.d("SUBSCRIPTION_RESPONSE", "mOnCompleteFlag ::  " + mOnCompleteFlag + " MssengerList Subscription completed" + "test :: " + test + " // mSubScriptionListener :: null");
            else
                Log.d("SUBSCRIPTION_RESPONSE", "mOnCompleteFlag ::  " + mOnCompleteFlag + " MssengerList Subscription completed" + "test :: " + test + " // mSubScriptionListener :: not null");


            if ( mSubScriptionListener != null && test ) {
                test = false;
                mSubScriptionListener.onComplete();
//                mSubScriptionListener = null;
//                subscriptionWatcher.cancel();
//                subscriptionWatcher = null;
//                subscriptionCallback = null;
            }
        }
    };


    private boolean test = true;



    // MessageList Query
    private void getMsgList(){
        ResponseFetcher type;
        if ( mOnCompleteFlag ) {
            type = AppSyncResponseFetchers.CACHE_AND_NETWORK;
        }
        else {
            type = AppSyncResponseFetchers.NETWORK_ONLY;
        }

        mAWSAppSyncClient.query(
                GetMyConversationQuery
                        .builder()
                        .first(10)
                        .build()
        )
                .responseFetcher(type)
                .enqueue(eventsCallback);
    }


    // MessageList Query
    public void getNextMsgList(){

        ResponseFetcher type;
        if ( mOnCompleteFlag ) {
            type = AppSyncResponseFetchers.CACHE_AND_NETWORK;
        }
        else {
            type = AppSyncResponseFetchers.NETWORK_ONLY;
        }

        if (nextToken != null) {
            mAWSAppSyncClient.query(
                    GetMyConversationQuery
                            .builder()
                            .first(10)
                            .after(nextToken)
                            .build()
            )
                    .responseFetcher(type)
                    .enqueue(eventsCallback);
        }
    }


    private GraphQLCall.Callback<GetMyConversationQuery.Data> eventsCallback = new GraphQLCall.Callback<GetMyConversationQuery.Data>() {
        @Override
        public void onResponse(@Nonnull Response<GetMyConversationQuery.Data> response) {
            if (response.data() != null) {

                //Network 일 경우 cache 리스트 제거
                if ( !response.fromCache() ) {
                    Iterator<MessengerListVo> i = result.iterator();
                    while (i.hasNext()) {
                        boolean cache = i.next().isCache();
                        if (cache) {
                            i.remove();
                        }
                    }
                }


                ArrayList<MessengerListUserVo> userList = null;
                List<GetMyConversationQuery.UserConversation> mConversationList;
                List<GetMyConversationQuery.Associated> mConversationUserInfoList;
                mConversationList = response.data().me().conversations().userConversations();

                for ( GetMyConversationQuery.UserConversation obj : mConversationList ) {
                    MessengerListVo listVo = new MessengerListVo();
                    listVo.setConversationId(obj.conversationId());
                    listVo.setUnreadcount(obj.unreadcount() );
                    listVo.setLastMessageTime(obj.lastMessageTime());
                    listVo.setLastMessage(obj.lastMessage());
                    listVo.setCache(response.fromCache());

                    mConversationUserInfoList = obj.associated();

                    userList = new ArrayList<MessengerListUserVo>();
                    for ( GetMyConversationQuery.Associated userObj : mConversationUserInfoList ){

                        MessengerListUserVo listUserVo = new MessengerListUserVo();
                        listUserVo.setId(userObj.user().cognitoId());
                        listUserVo.setProfileImage(userObj.user().profileImage());
                        listUserVo.setUsername(userObj.user().username());
                        listUserVo.setCustomData(userObj.user().customData());

                        // 본인 정보 제외
                        if ( userObj.user().cognitoId().equals(mUsersQuery.getMyCognitoId())) {
                            listUserVo.setMe(true);
                        }
                        else {
                            listUserVo.setMe(false);
                        }
                        userList.add(listUserVo);


                    }
                    listVo.setUserVo( userList );
                    result.add( listVo );
                }

                nextToken = response.data().me().conversations().nextToken();
                mMsgListListener.onResponse(result);

            } else {
                //events = new ArrayList<>();
            }
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            if ( mMsgListListener != null ) {
                mMsgListListener.onFailure(e);
            }
        }
    };
}
