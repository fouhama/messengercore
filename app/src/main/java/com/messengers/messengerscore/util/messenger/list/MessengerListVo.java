package com.messengers.messengerscore.util.messenger.list;

import java.util.ArrayList;

public class MessengerListVo {

    private String conversationId;
    private int unreadcount;
    private String lastMessage;
    private String lastMessageTime;
    private boolean cache;
    private ArrayList<MessengerListUserVo> userVo;

    public boolean isCache() {
        return cache;
    }

    public void setCache(boolean cache) {
        this.cache = cache;
    }

    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }

    public String getLastMessageTime() {
        return lastMessageTime;
    }

    public void setLastMessageTime(String lastMessageTime) {
        this.lastMessageTime = lastMessageTime;
    }

    public String getConversationId() {
        return conversationId;
    }

    public void setConversationId(String conversationId) {
        this.conversationId = conversationId;
    }

    public int getUnreadcount() {
        return unreadcount;
    }

    public void setUnreadcount(int unreadcount) {
        this.unreadcount = unreadcount;
    }

    public ArrayList<MessengerListUserVo> getUserVo() {
        return userVo;
    }

    public void setUserVo(ArrayList<MessengerListUserVo> userVo) {
        this.userVo = userVo;
    }
}
