package com.messengers.messengerscore.util.messenger.read;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.messengers.graphql.ReadMutation;
import com.messengers.messengerscore.util.ClientFactory;
import com.messengers.messengerscore.util.query.UsersQuery;

import javax.annotation.Nonnull;

public class Reader {

    private CognitoUserPoolsAuthProvider mAuthProvider;
    private Context mCtx;
    private String mConversationId;
    private UsersQuery mUsersQuery;

    public Reader(Context _mCtx , CognitoUserPoolsAuthProvider _mAuthProvider ,  UsersQuery _mUsersQuery){
        this.mAuthProvider = _mAuthProvider;
        this.mCtx = _mCtx;
        this.mUsersQuery = _mUsersQuery;
    }

    public void read( String _mConversationId ){
        this.mConversationId = _mConversationId;

        ReadMutation readMsg = ReadMutation.builder()
                .conversationId(mConversationId)
                .token(mAuthProvider.getLatestAuthToken())
                .userId(mUsersQuery.getMyCognitoId())
                .build();

        ClientFactory.getInstance(mAuthProvider , mCtx )
                .mutate(readMsg)
                .enqueue(messageReadCallback);

    }

    /**
     * Service response subscriptionCallback confirming receipt of new comment triggered by UI.
     */
    private GraphQLCall.Callback<ReadMutation.Data> messageReadCallback = new GraphQLCall.Callback<ReadMutation.Data>() {
        @Override
        public void onResponse(@Nonnull Response<ReadMutation.Data> response) {
            Log.e("Reder Response" , "Suc !!");
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
            Log.e("Reder Response" , "Fail !!" + e.getMessage());
        }
    };
}
