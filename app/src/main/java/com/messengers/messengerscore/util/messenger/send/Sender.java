//package com.messengers.messengerscore.util.messenger.send;
//
//import android.content.Context;
//import android.os.Handler;
//import android.os.Looper;
//import android.util.Log;
//
//import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
//import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
//import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
//import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
//import com.amazonaws.mobileconnectors.cognitoidentityprovider.util.CognitoJWTParser;
//import com.apollographql.apollo.GraphQLCall;
//import com.apollographql.apollo.api.Response;
//import com.apollographql.apollo.exception.ApolloException;
//import com.messengers.graphql.CreateMessageMutation;
//import com.messengers.graphql.GetMessageQuery;
//import com.messengers.graphql.MessageSubscription;
//import com.messengers.messengerscore.util.ClientFactory;
//import com.messengers.messengerscore.util.messenger.list.MessengerListUserVo;
//
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.List;
//
//import javax.annotation.Nonnull;
//
//public class Sender {
//
//    private CognitoUserPoolsAuthProvider mAuthProvider;
//    private Context mCtx;
//    private String mContent;
//    private String mConversationId;
//
//    public Sender(Context _mCtx , CognitoUserPoolsAuthProvider _mAuthProvider){
//        this.mAuthProvider = _mAuthProvider;
//        this.mCtx = _mCtx;
//    }
//
//    public void send(String _mContent , String _mConversationId){
//        this.mContent = _mContent;
//        this.mConversationId = _mConversationId;
//
//        CreateMessageMutation.Data expected = new CreateMessageMutation.Data(new CreateMessageMutation.CreateMessage(
//                "",
//                "",
//                mContent,
//                "",
//                "",
//                false,
//                mConversationId
//        ));
//
//        CreateMessageMutation sendMsg = CreateMessageMutation.builder()
//                .content(mContent)
//                .conversationId(mConversationId)
//                .token(mAuthProvider.getLatestAuthToken())
//                .build();
//
//        //addPostOffline(expected);
//        ClientFactory.getInstance(mAuthProvider , mCtx )
//                .mutate(sendMsg )
//                .enqueue(messageSendCallback);
//    }
//
//    private void addPostOffline(final CreateMessageMutation.Data addPostData) {
//        final AWSAppSyncClient appSyncClient = ClientFactory.getInstance(mAuthProvider , mCtx);
//        final GetMessageQuery allPostsQuery = GetMessageQuery.builder().conversationId(mConversationId).build();
//        appSyncClient.query(allPostsQuery)
//                .responseFetcher(AppSyncResponseFetchers.CACHE_ONLY)
//                .enqueue(new GraphQLCall.Callback<GetMessageQuery.Data>() {
//                    @Override
//                    public void onResponse(@Nonnull Response<GetMessageQuery.Data> response) {
//
//                        List<GetMessageQuery.Message> oldPostsList = response.data().allMessageConnection().messages();
//                        List<GetMessageQuery.Message> newPostsList = new ArrayList<>(oldPostsList);
//
//                        newPostsList.add(new GetMessageQuery.Message(
//                                addPostData.createMessage().__typename(),
//                                addPostData.createMessage().id(),
//                                addPostData.createMessage().content(),
//                                addPostData.createMessage().isSent(),
//                                addPostData.createMessage().sender(),
//                                addPostData.createMessage().conversationId()
//                        ));
//
//                        GetMessageQuery.Data data =
//                                new GetMessageQuery.Data
//                                        (new GetMessageQuery.AllMessageConnection(
//                                                response.data().allMessageConnection().__typename(), newPostsList, null)
//                                        );
//
//                        try {
//                            appSyncClient.getStore().write(allPostsQuery, data).execute();
//                        } catch (ApolloException e) {
//                            Log.e("As", "Failed to update ListPosts query optimistically", e);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(@Nonnull ApolloException e) {
//                        Log.e("as", "Failed to update ListPosts query optimistically", e);
//                    }
//                });
//    }
//
//
//
//
//    /**
//     * Service response subscriptionCallback confirming receipt of new comment triggered by UI.
//     */
//    private GraphQLCall.Callback<CreateMessageMutation.Data> messageSendCallback = new GraphQLCall.Callback<CreateMessageMutation.Data>() {
//        @Override
//        public void onResponse(@Nonnull Response<CreateMessageMutation.Data> response) {
//        }
//
//        @Override
//        public void onFailure(@Nonnull ApolloException e) {
//        }
//    };
//}


package com.messengers.messengerscore.util.messenger.send;

import android.content.Context;
import android.util.Log;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.api.internal.Utils;
import com.apollographql.apollo.exception.ApolloException;
import com.messengers.graphql.CreateMessageMutation;
import com.messengers.graphql.GetMessageQuery;
import com.messengers.messengerscore.util.ClientFactory;
import com.messengers.messengerscore.util.query.UsersQuery;
import com.messengers.messengerscore.util.util.RandomId;

import net.danlew.android.joda.JodaTimeAndroid;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

public class Sender {

    private CognitoUserPoolsAuthProvider mAuthProvider;
    private Context mCtx;
    private String mContent;
    private String mConversationId;
    private UsersQuery usersQuery;

    public Sender(Context _mCtx , CognitoUserPoolsAuthProvider _mAuthProvider , UsersQuery _usersQuery ){
        this.mAuthProvider = _mAuthProvider;
        this.mCtx = _mCtx;
        this.usersQuery = _usersQuery;
        JodaTimeAndroid.init(_mCtx);
    }

    public void send(String _mContent , String _mConversationId){
        this.mContent = _mContent;
        this.mConversationId = _mConversationId;


        String senderId = RandomId.getSendMsgId().toString();
        DateTime dt = new DateTime(DateTimeZone.UTC);

        CreateMessageMutation.Data expected = new CreateMessageMutation.Data(
                new CreateMessageMutation.CreateMessage(
                        "Message",
                        senderId,
                        mContent,
                        usersQuery.getMyCognitoId(),
                        dt.toString(),
                        false,
                        mConversationId
        ));


        CreateMessageMutation sendMsg = CreateMessageMutation.builder()
                .id(senderId)
                .content(mContent)
                .conversationId(mConversationId)
                .token(mAuthProvider.getLatestAuthToken())
                .build();

        addPostOffline(expected);


        ClientFactory.getInstance(mAuthProvider , mCtx )
                .mutate(sendMsg )
                .enqueue(messageSendCallback);

    }

    private void addPostOffline(final CreateMessageMutation.Data addPostData) {
        final AWSAppSyncClient appSyncClient = ClientFactory.getInstance(mAuthProvider , mCtx);
        final GetMessageQuery allPostsQuery = GetMessageQuery.builder().conversationId(mConversationId).build();
        appSyncClient.query(allPostsQuery)
                .responseFetcher(AppSyncResponseFetchers.CACHE_ONLY)
                .enqueue(new GraphQLCall.Callback<GetMessageQuery.Data>() {
                    @Override
                    public void onResponse(@Nonnull Response<GetMessageQuery.Data> response) {

                        List<GetMessageQuery.Message> oldPostsList = response.data().allMessageConnection().messages();
                        List<GetMessageQuery.Message> newPostsList = new ArrayList<>(oldPostsList);

                        newPostsList.add(0, new GetMessageQuery.Message(
                                addPostData.createMessage().__typename(),
                                addPostData.createMessage().id(),
                                addPostData.createMessage().content(),
                                addPostData.createMessage().isSent(),
                                addPostData.createMessage().createdAt(),
                                addPostData.createMessage().sender()
                        ));

                        GetMessageQuery.Data data =
                                new GetMessageQuery.Data(new GetMessageQuery.AllMessageConnection(
                                                response.data().allMessageConnection().__typename(), newPostsList, null));

                        try {
                            appSyncClient.getStore().write(allPostsQuery, data).execute();
                        } catch (ApolloException e) {
                            Log.e("As", "Failed to update ListPosts query optimistically", e);
                        }
                    }

                    @Override
                    public void onFailure(@Nonnull ApolloException e) {
                        Log.e("as", "Failed to update ListPosts query optimistically", e);
                    }
                });
    }




    /**
     * Service response subscriptionCallback confirming receipt of new comment triggered by UI.
     */
    private GraphQLCall.Callback<CreateMessageMutation.Data> messageSendCallback = new GraphQLCall.Callback<CreateMessageMutation.Data>() {
        @Override
        public void onResponse(@Nonnull Response<CreateMessageMutation.Data> response) {

//            addPostOffline(response.data());

//            String _typeName = response.data().createMessage().__typename();
//            String id = response.data().createMessage().id();
//            String conversationId = response.data().createMessage().conversationId();
//            String sender = response.data().createMessage().sender();
//            String content = response.data().createMessage().content();
//            String createAt = response.data().createMessage().createdAt();
//            boolean isSent = response.data().createMessage().isSent();
//
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: _typeName :: " + _typeName);
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: id :: " + id);
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: conversationId :: " + conversationId);
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: sender :: " + sender);
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: content :: " + content);
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: createAt :: " + createAt);
//            Log.e("MESSENGER CORE" , "CREATEMESSAGE MUTATION :: isSent :: " + isSent);


//            final AWSAppSyncClient appSyncClient = ClientFactory.getInstance(mAuthProvider , mCtx);
//            String _typeName = response.data().createMessage().__typename();
//            String id = response.data().createMessage().id();
//            String conversationId = response.data().createMessage().conversationId();
//            String sender = response.data().createMessage().sender();
//            String content = response.data().createMessage().content();
//            String createAt = response.data().createMessage().createdAt();
//            boolean isSent = response.data().createMessage().isSent();
//
//            List<CreateMessageMutation.Data> optimisticResponse = new ArrayList<>();
//
//
////                public Message(@Nonnull String __typename, @Nonnull String id, @Nonnull String content,
////            boolean isSent, @Nonnull String createdAt, @Nonnull String sender) {
////                this.__typename = Utils.checkNotNull(__typename, "__typename == null");
////                this.id = Utils.checkNotNull(id, "id == null");
////                this.content = Utils.checkNotNull(content, "content == null");
////                this.isSent = isSent;
////                this.createdAt = Utils.checkNotNull(createdAt, "createdAt == null");
////                this.sender = Utils.checkNotNull(sender, "sender == null");
////            }
//
//
//
//
////            CreateMessageMutation.Data data =
////                    new CreateMessageMutation.Data
////                            (new GetMessageQuery.Message(
////                                    response.data().createMessage().__typename(), id,content , isSent, createAt, sender)
////                            );
//
//
//            CreateMessageMutation.Data data = new CreateMessageMutation.Data(
//                    new CreateMessageMutation.CreateMessage(
//                            _typeName, id, content, sender, createAt, isSent , conversationId
//                    )
//            );
//
//
//            appSyncClient.getStore().write(allPostsQuery, data).execute();

        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
        }
    };
}
