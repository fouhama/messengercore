package com.messengers.messengerscore.util.messenger.totalunreadcnt;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.messengers.graphql.TotalUnreadQuery;
import com.messengers.messengerscore.util.listener.OnUnreadTotalCntListener;
import com.messengers.messengerscore.util.query.UsersQuery;


import javax.annotation.Nonnull;

public class ToatlUnreadCnt {

    private AWSAppSyncClient mAWSAppSyncClient;
    private UsersQuery mUsersQuery;
    private OnUnreadTotalCntListener mUnreadCntListener;

    public ToatlUnreadCnt(AWSAppSyncClient _mAWSAppSyncClient , UsersQuery _mUsersQuery){
        this.mAWSAppSyncClient = _mAWSAppSyncClient;
        this.mUsersQuery = _mUsersQuery;
    }

    public void setUnreadCntListListener (OnUnreadTotalCntListener _mUnreadCntListener){
        this.mUnreadCntListener = _mUnreadCntListener;
    }

    public void getTotalCnt(){
        mAWSAppSyncClient.query(
                TotalUnreadQuery
                        .builder()
                        .userId(mUsersQuery.getMyCognitoId())
                        .build()
        )
                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
                .enqueue(eventsCallback);
    }

    private GraphQLCall.Callback<TotalUnreadQuery.Data> eventsCallback = new GraphQLCall.Callback<TotalUnreadQuery.Data>() {
        @Override
        public void onResponse(@Nonnull Response<TotalUnreadQuery.Data> response) {
            if (response.data() != null) {
                if ( mUnreadCntListener != null ) {
                    int cnt = response.data().getTotalUnread();
                    mUnreadCntListener.onUnreadCnt(cnt);
                }
            }
        }

        @Override
        public void onFailure(@Nonnull ApolloException e) {
        }
    };

}
