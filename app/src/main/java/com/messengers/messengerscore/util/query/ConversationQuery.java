package com.messengers.messengerscore.util.query;

import android.content.Context;

import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;

public class ConversationQuery extends Query {

    public ConversationQuery(Context context, CognitoUserPoolsAuthProvider authProvider) {
        super(context, authProvider);
    }
}
