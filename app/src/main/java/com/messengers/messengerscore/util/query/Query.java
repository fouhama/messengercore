package com.messengers.messengerscore.util.query;

import android.content.Context;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.messengers.messengerscore.util.ClientFactory;

class Query {
    private Context mContext;
    private CognitoUserPoolsAuthProvider mAuthProvider;
    private AWSAppSyncClient mAWSAppSyncClient;

    public Query(Context context, CognitoUserPoolsAuthProvider authProvider) {
        mContext = context;
        mAuthProvider = authProvider;
    }

    AWSAppSyncClient getClient() {
        return ClientFactory.getInstance(mAuthProvider, mContext);
    }
}

//class ConversationQuery extends Query {
//
//    ConversationQuery(Context context, CognitoUserPoolsAuthProvider authProvider) {
//        super(context, authProvider);
//    }
////
////    void getUserConversation() {
////        AWSAppSyncClient client = getClient();
////        client.query(UserConversationsQuery.builder()
////                .nextToken(null)
////                .build())
////                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
////                .enqueue(new GraphQLCall.Callback<UserConversationsQuery.Data>() {
////                    @Override
////                    public void onResponse(@Nonnull Response<UserConversationsQuery.Data> response) {
////                        Log.i("HAMA", "get conversations");
////                        Log.i("HAMA", response.fromCache() + "");
////                        if (response.data() == null) {
////                            Log.i("HAMA", "null");
////                        } else {
////                            UserConversationsQuery.Conversations conversations = response.data().me().conversations();
////                            List<UserConversationsQuery.UserConversation> items = conversations.userConversations();
////                            for (UserConversationsQuery.UserConversation item : items) {
////                                Log.i("HAMA", item.conversation().id());
////                                Log.i("HAMA", item.conversation().createdAt());
////
////                                List<UserConversationsQuery.Associated> associateds = item.associated();
////                                Log.i("HAMA", "associated");
////                                for (UserConversationsQuery.Associated associated : associateds) {
////                                    Log.i("HAMA", associated.user().username());
////                                }
////                            }
////
////                        }
////                    }
////
////                    @Override
////                    public void onFailure(@Nonnull ApolloException e) {
////                        Log.e("HAMA", "Failed to make events api call", e);
////                        Log.e("HAMA", e.getMessage());
////                    }
////                });
////    }
//}

//class UsersQuery extends Query {
//    UsersQuery(Context context, CognitoUserPoolsAuthProvider authProvider) {
//        super(context, authProvider);
//    }
//
////    void me() {
////        AWSAppSyncClient AWSAppSyncClient = getClient();
////        AWSAppSyncClient.query(MeQuery.builder().build())
////                .responseFetcher(AppSyncResponseFetchers.CACHE_AND_NETWORK)
////                .enqueue(new GraphQLCall.Callback<MeQuery.Data>() {
////                    @Override
////                    public void onResponse(@Nonnull Response<MeQuery.Data> response) {
////                        Log.i("HAMA", response.fromCache() + "");
////                        if (response.data() == null) {
////                            Log.i("HAMA", "null");
////                        } else {
////                            Log.i("HAMA", response.data().me().cognitoId());
////                        }
////                    }
////
////                    @Override
////                    public void onFailure(@Nonnull ApolloException e) {
////                        Log.e("HAMA", "Failed to make events api call", e);
////                        Log.e("HAMA", e.getMessage());
////                    }
////                });
////    }
//}
