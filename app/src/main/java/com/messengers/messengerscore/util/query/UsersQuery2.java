package com.messengers.messengerscore.util.query;

import android.content.Context;

import com.amazonaws.mobileconnectors.appsync.AWSAppSyncClient;
import com.amazonaws.mobileconnectors.appsync.fetcher.AppSyncResponseFetchers;
import com.amazonaws.mobileconnectors.appsync.sigv4.CognitoUserPoolsAuthProvider;
import com.apollographql.apollo.GraphQLCall;
import com.apollographql.apollo.exception.ApolloException;
import com.messengers.graphql.GetMyCognitoIdQuery;
import com.messengers.messengerscore.util.listener.OnCognitoistener;

import javax.annotation.Nonnull;

public class UsersQuery2 extends Query {

    private String myCognitoId;
    private OnCognitoistener listener;

    public UsersQuery2(Context context, CognitoUserPoolsAuthProvider authProvider ) {
        super(context, authProvider);
        //this.listener = _listener;
        //myCognitoId();
    }

    public void setListener( OnCognitoistener _listener){
        this.listener = _listener;
    }

    public void query(OnCognitoistener listener) {
        //
    }

    private void myCognitoId() {
        AWSAppSyncClient AWSAppSyncClient = getClient();
        AWSAppSyncClient.query(GetMyCognitoIdQuery.builder().build())
                .responseFetcher(AppSyncResponseFetchers.NETWORK_ONLY)
                .enqueue(new GraphQLCall.Callback<GetMyCognitoIdQuery.Data>() {
                    @Override
                    public void onResponse(@Nonnull com.apollographql.apollo.api.Response<GetMyCognitoIdQuery.Data> response) {
                        if ( response.data() != null ) {
                            myCognitoId = response.data().me().cognitoId();
                            if ( listener != null) {
                                listener.onSuccessCognito();
                            }
                        }
                    }

                    @Override
                    public void onFailure(@Nonnull ApolloException e) {
                        listener.onFailCognito(e);
                    }
                });
    }


    public String getMyCognitoId(){
        return myCognitoId;
    }
}
