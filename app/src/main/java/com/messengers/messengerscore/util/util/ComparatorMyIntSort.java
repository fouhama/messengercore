package com.messengers.messengerscore.util.util;

import java.util.Comparator;
import java.util.Date;

public class ComparatorMyIntSort implements Comparator<Date> {
    @Override
    public int compare(Date t1, Date t2) {

        int compare = t1.compareTo(t2);
        if ( compare > 0 ){
            return  -1;
        }

        else if ( compare == 0 ){
            return  0;
        }

        else {
            return  1;
        }
    }
}
