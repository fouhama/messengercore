package com.messengers.messengerscore.util.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateTime {

    public static Date UTCConvertGMT(String DATE){

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault());
        Date date = null;
        try {
            date = format.parse(DATE);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return  dateFromUTC( date );

    }

    public static Date dateFromUTC(Date date){
        return new Date(date.getTime() + Calendar.getInstance().getTimeZone().getOffset(date.getTime()));
    }

}
