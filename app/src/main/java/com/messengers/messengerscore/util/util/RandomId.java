package com.messengers.messengerscore.util.util;

import java.util.Random;

public class RandomId {

    public static StringBuffer getSendMsgId(){
        StringBuffer id = new StringBuffer();
        Random rnd = new Random();
        for (int i = 0; i< 36; i++){
            id.append((char) ((int) (rnd.nextInt(26)) + 97));
        }
        return  id;
    }
}
